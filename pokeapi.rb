require "json"
require 'faraday'

$pokeapi_url = "https://pokeapi.co/api/v2/"

class Pokemon
  def initialize(data)
    @json = data
    @height = data["height"]
    @id = data["id"]
    @name = data["name"]
    @sprites = data["sprites"]
    @species = Species.get(data["species"]["name"])
    @types = []
    @abilities = []
    @forms = []
    @games = []
    for type in data["types"]
      @types.append(Type.get(type["type"]["name"]))
    end
  end
  attr_reader :height
  attr_reader :id
  attr_reader :name
  attr_reader :sprites
  attr_reader :species
  attr_reader :types
  attr_reader :abilities
  attr_reader :forms
  attr_reader :games

  def encounters
    if File.exists? "#{Dir.home}/.pokeapi/encounters/#{@name}.json"
      body = JSON.parse(File.read("#{Dir.home}/.pokeapi/encounters/#{@name}.json"))
    else
      puts "Downloading encounters..."
      Dir.mkdir("#{Dir.home}/.pokeapi/encounters/") unless Dir.exists? "#{Dir.home}/.pokeapi/encounters/"
      body = JSON.parse(Faraday.get($pokeapi_url+"pokemon/#{@name}/encounters").body)
      File.open("#{Dir.home}/.pokeapi/encounters/#{@name}.json", "w+") { |file| file.write(body.to_json) }
    end
    
    @locations = []
    
    for loc in body
      @locations.append EncounterLocation.new(loc)
    end
    return @locations   
  end

  def sprite(type = :front_default)
    options = [:back_default, :back_female, :back_shiny, :back_shiny_female,
               :front_default, :front_female, :front_shiny, :front_shiny_female ]
    
    if options.include? type
      type = type.to_s

      if File.exists? "#{Dir.home}/.pokeapi/sprite/#{type}_#{@id}.png"
        return Sprite.new("#{Dir.home}/.pokeapi/sprite/#{type}_#{@id}.png")
      end

      Dir.mkdir("#{Dir.home}/.pokeapi/sprite/") unless Dir.exists? "#{Dir.home}/.pokeapi/sprite/"
      puts "Downloading sprite..."
      body = Faraday.get(@sprites[type]).body
      File.open("#{Dir.home}/.pokeapi/sprite/#{type}_#{@id}.png", "w+") { |file| file.write(body)  }
      return Sprite.new("#{Dir.home}/.pokeapi/sprite/#{type}_#{@id}.png")     
    end
  end
  
  def to_s; "##{@id} #{@name}"; end
  def Pokemon.get(name); return PokeAPI::get(:pokemon, name); end
end

class EncounterLocation
  def initialize(data)
    @json = data
    @area = Area.get(data["location_area"]["name"])
    @encounters = []
    for v in data["version_details"]
      for enc in v["encounter_details"]
        @encounters.append Encounter.new(enc, v["version"]["name"], v["max_chance"])
      end
    end
  end
  attr_reader :area
  attr_reader :encounters
  attr_reader :version
  def to_s; "#{@encounters}"; end
end

class Encounter
  def initialize(data, version, max_chance)
    @json = data
    @chance = data["chance"]
    @max_chance = max_chance
    @maxlevel = data["max_level"]
    @minlevel = data["min_level"]
    @method = data["method"]["name"]
    @conditions = []
    for cond in data["condition_values"]; @conditions.append cond["name"]; end
    @version = version
  end
  attr_reader :chance
  attr_reader :maxlevel
  attr_reader :minlevel
  attr_reader :conditions
  attr_reader :method
  attr_reader :version
  attr_reader :max_chance
end

class Area
  def initialize(data)
    @json = data
    @id = data["id"]
    @name = data["name"]
    @location = data["location"]["name"]
  end
  def location
    return Location.get(@location)
  end
  attr_reader :name
  attr_reader :id
  def to_s; "##{@id} #{@name} in #{@location}"; end
  def Area.get(name); return PokeAPI::get("location-area", name); end
end

class Location
  def initialize(data)
    @json = data
    @areas = []
    @id = data["id"]
    @name = data["name"]
    @region = data["region"]["name"]
    for area in data["areas"]
      @areas.append area["name"]
    end
  end
  attr_reader :areas
  attr_reader :id
  attr_reader :name
  attr_reader :region
  def Location.get(name); return PokeAPI::get("location", name); end
  def to_s; "##{@id} #{@name} in the #{@region} region"; end
end


class Sprite
  def initialize(path); @path = path; @image = File.read(path); end
  def save(path); File.open(path, "w+") { |file| file.write(@image) }; end
  attr_reader :path
  attr_reader :image
end

class Species
  def initialize(data)
    @json = data
    @name = data["name"]
    @id = data["id"]
    @color = data["color"]["name"]
    @colour = @color
  end
  attr_reader :colour
  attr_reader :color
  attr_reader :name
  attr_reader :id
  def to_s; "##{@id} #{@name} #{color} species"; end
  def Species.get(name); return PokeAPI::get("pokemon-species", name); end
end

class Type
  def initialize(data)
    @json = data
    @name = data["name"]
    @id = data["id"]
    @type_class = data["move_damage_class"]["name"]
    @moves = []
    @pokemon = []
    @double_damage_from = []
    @double_damage_to = []
    @half_damage_from = []
    @half_damage_to = []
    @no_damage_from = []
    @no_damage_to = []

    for ddf in data["damage_relations"]["double_damage_from"]
      @double_damage_from.append ddf["name"]
    end
    
    for ddt in data["damage_relations"]["double_damage_to"]
      @double_damage_to.append ddt["name"]
    end
    
    for hdf in data["damage_relations"]["half_damage_from"]
      @half_damage_from.append hdf["name"]
    end
    
    for hdt in data["damage_relations"]["half_damage_to"]
      @half_damage_to.append hdt["name"]
    end
    
    for ndf in data["damage_relations"]["no_damage_from"]
      @no_damage_from.append ndf["name"]
    end
    
    for ndt in data["damage_relations"]["no_damage_to"]
      @no_damage_to.append ndt["name"]
    end
    
    for mv in data["moves"]
      @moves.append mv["name"]
    end
    for pkmn in data["pokemon"]
      @pokemon.append pkmn["pokemon"]["name"]
    end
  end

  def double_damage_from?(type); return @double_damage_from.include? type.to_s; end
  def half_damage_from?(type); return @half_damage_from.include? type.to_s; end
  def no_damage_from?(type); return @no_damage_from.include? type.to_s; end
  def double_damage_to?(type); return @double_damage_to.include? type.to_s; end
  def half_damage_to?(type); return @half_damage_to.include? type.to_s; end
  def no_damage_to?(type); return @no_damage_to.include? type.to_s; end
  
  attr_reader :name
  attr_reader :id
  attr_reader :type_class
  attr_reader :moves
  attr_reader :pokemon
  attr_reader :double_damage_to
  attr_reader :double_damage_from
  attr_reader :half_damage_from
  attr_reader :half_damage_to
  attr_reader :no_damage_to
  attr_reader :no_damage_from
       
  def to_s; "#{@name}"; end
  def Type.get(name); return PokeAPI::get(:type, name); end
end

class Move
  def initialize(data)
    @json = data
    @name = data["name"]
    @id = data["id"]
  end
  def to_s; "##{@id} #{@name}"; end
  def Move.get(name); return PokeAPI::get(:move, name); end
end

class Ability
end

class Item
end

module PokeAPI
  def PokeAPI.isCached?(type, name)
    Dir.mkdir("#{Dir.home}/.pokeapi/") unless Dir.exists? "#{Dir.home}/.pokeapi/"
    return true if File.exist? "#{Dir.home}/.pokeapi/#{type}/#{name}.json"
    return false
  end

  def PokeAPI.getCached(type, name);
    if isCached? type, name
      JSON.parse(File.read("#{Dir.home}/.pokeapi/#{type}/#{name}.json"))
    end
  end

  def PokeAPI.cache(type, name, object)
    Dir.mkdir("#{Dir.home}/.pokeapi/") unless Dir.exists? "#{Dir.home}/.pokeapi/"
    Dir.mkdir("#{Dir.home}/.pokeapi/#{type}/") unless Dir.exists? "#{Dir.home}/.pokeapi/#{type}/"

    unless File.exists? "#{Dir.home}/.pokeapi/#{type}/#{name}.json"
      File.open("#{Dir.home}/.pokeapi/#{type}/#{name}.json", "w+") { |file| file.write(object.to_json) }
    end

    return object
  end
  
  def PokeAPI.allPokemon; JSON.parse(Faraday.get("#{$pokeapi_url}pokemon").body); end
  
  def PokeAPI.get(type, name)
    name = name.to_s
    type = type.to_s
    if PokeAPI::isCached? type, name;
      res = PokeAPI::getCached(type, name);
    else
      puts "Pulling live content..."
      j = Faraday.get("#{$pokeapi_url}#{type.to_s}/#{name}").body
      if j == "Not Found"
        return nil
      end
      res = JSON.parse j
    end
    
    if type == "pokemon"
      return Pokemon.new(PokeAPI::cache(type, name, res))
    elsif type == "pokemon-species"
      return Species.new(PokeAPI::cache(type, name, res))
    elsif type == "type"
      return Type.new(PokeAPI::cache(type, name, res))
    elsif type == "move"
      return Move.new(PokeAPI::cache(type, name, res))
    elsif type == "location"
      return Location.new(PokeAPI::cache(type, name, res))     
    elsif type == "location-area"
      return Area.new(PokeAPI::cache(type, name, res))
    end
  end
end
