require "gtk2"
require "pokeapi"

class Pokedex
  def initialize
    @root = Gtk::Window.new
    @root << @vert = Gtk::VBox.new
    @display = nil
    @root.title = "Pokedex"
    @root.signal_connect('destroy') { Gtk.main_quit }
    @vert << @ctrls = Gtk::HBox.new
    @ctrls << @mode = Gtk::Button.new("Pokemon")
    @ctrls << @search = Gtk::Entry.new
    @ctrls << @sender = Gtk::Button.new("Go!")

    menu = Gtk::Menu.new
    menu.append(mitem1 = Gtk::MenuItem.new("Pokemon"))
    menu.append(mitem2 = Gtk::MenuItem.new("Types"))
    menu.show_all
    mitem1.signal_connect('activate') { |w| @mode.label = "Pokemon" }
    mitem2.signal_connect('activate') { |w| @mode.label = "Type" }
    @sender.signal_connect("clicked") { |w| loadUI @mode.label.downcase, @search.text }
    @mode.signal_connect("clicked") { |w| menu.popup(nil, nil, 0, 0) }
    @root.show_all
  end

  def loadUI endpoint, search
    if search == ""
      return
    end
    @display.destroy if @display != nil
    @error.destroy if @error != nil
    if endpoint == "pokemon"
      pokemon = Pokemon.get(search)
      if pokemon == nil
        @vert << @error = Gtk::Label.new("Resource #{endpoint}:#{search} not found.")
        @error.signal_connect('destroy') { @error = nil }
        @error.show
        return
      end
      
      @display = Gtk::Table.new 6, 4, false
      @vert <<  @display
      @display.signal_connect('destroy') { @display = nil }
      
      @display.attach(@sprites = Gtk::Image.new(pokemon.sprite.path), 0, 2, 0, 2)
      @display.attach(Gtk::Label.new(pokemon.name), 2, 6, 0, 1)
      @display.attach(Gtk::Label.new(pokemon.types.join(", ")), 2, 6, 1, 2)
      @display.attach(@sprite_label = Gtk::Label.new("front default"), 0, 2, 2, 3)
      @display.attach(@evo = Gtk::Button.new("Evolution Tree"), 2, 6, 2, 3)
      @display.attach(@enc = Gtk::Button.new("Encounters"), 2, 6, 3, 4)
      
      @sprite_types = [:back_default, :back_female, :back_shiny, :back_shiny_female,
                       :front_default, :front_female, :front_shiny, :front_shiny_female]
      
      @sprite_sel = 4
      @display.attach(@bs = Gtk::Button.new("<"), 0, 1, 3, 4)
      @display.attach(@bf = Gtk::Button.new(">"), 1, 2, 3, 4)
      @bs.signal_connect('clicked') { |w|
        @sprite_sel -= 1
        @sprite_sel = @sprite_types.length - 1 if @sprite_sel < 0
        puts @sprite_types[@sprite_sel]
        @sprite_label.text = @sprite_types[@sprite_sel].to_s.gsub("_", " ")
        nsp = pokemon.sprite(@sprite_types[@sprite_sel]).path
        puts nsp
        @sprites.file = nsp
      }
      @bf.signal_connect('clicked') { |w|
        @sprite_sel += 1
        @sprite_sel = 0 if @sprite_sel > @sprite_types.length - 1
        puts @sprite_types[@sprite_sel]
        @sprite_label.text = @sprite_types[@sprite_sel].to_s.gsub("_", " ")
        nsp = pokemon.sprite(@sprite_types[@sprite_sel]).path
        puts nsp
        @sprites.file = nsp
      }

      @display.show_all
    end

  end
end

class Pokemon
  def show(base_window = nil)
    if base_window == nil
      @base_window = Gtk::Window.new
    else
      @base_window = base_window
    end
  end
end

class Sprite
  def show(base_window = nil, standalone = false)
    if base_window == nil
      @base_window = Gtk::Window.new
      
    else
      @base_window = base_window
    end
    #spr = GdkPixbuf::Pixbuf.new(:file => @path);

    spr = Gtk::Image.new(@path);
    @base_window.add spr
    @base_window.show_all

    if standalone
      @base_window.signal_connect('destroy') { Gtk.main_quit }
      Gtk.main
    end
  end

  def selfContainedUI

  end
end
